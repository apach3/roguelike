﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //static instance allows us to access GameManager from any other script
    public static GameManager Instance = null;

    //hold reference to our BoardManager which will setup the level
    public BoardManager _boardScript;

    //current level number; in game expressed as "Day 1"
    private int _level = 3;

    /// <summary>
    /// Is always called before any Start function.
    /// Inititalise and setup our GameManager. Get BoardManager and Init the game.
    /// </summary>
	void Awake () {

        //does the instance already exists?
        if (Instance == null)
            //if not, set instance to this
            Instance = this;
        //instance already exitst but it's not this
        else if (Instance != this)
            //Destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of GameManager class.
            Destroy(gameObject);

        //Do not destroy this object when reloading scene
        DontDestroyOnLoad(gameObject);

        //Get component reference to the attached BoardManger
        _boardScript = GetComponent<BoardManager>();

        //Initialize first level
        InitGame();
	}
	
    /// <summary>
    /// Initializes the game for each level.
    /// </summary>
    void InitGame()
    {
        //Setup the scene for current level
        _boardScript.SetupScene(_level);
    }
}
