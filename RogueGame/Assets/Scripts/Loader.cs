﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

    //GameManger prefab to instntiate
    public GameManager _gameManager;
    //SoundManager prefab to instantiate
    //
    
    /// <summary>
    /// Load GameManger and SoundManger.
    /// </summary>
    void Awake () {
        //Is our GameManager instantiated?
		if (GameManager.Instance == null)
            //if not, instatiate it
            Instantiate(_gameManager);
	}
}
