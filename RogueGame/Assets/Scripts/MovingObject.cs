﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class lets us define incomplete members in our class. We will have to implement them in derived class.
/// </summary>
public abstract class MovingObject : MonoBehaviour {

    //time it will take object to move, in seconds
    public float MoveTime = 0.1f;
    //layer on which collision will be checked
    public LayerMask BlockingLayer;

    //the Boxcollider2D component attached to this object
    private BoxCollider2D _boxCollider;
    //the Rigidbody2D component attached to this object
    private Rigidbody2D _rb2D;
    //us to make movement more efficient
    private float _inverseMoveTime;

    /// <summary>
    /// Protected, virtual functions can be overridden by inherithing classes.
    /// </summary>
    protected virtual void Start() {
        //get a component reference to this object's BoxCollider2D
        _boxCollider = GetComponent<BoxCollider2D>();
        //get a component reference to this object's Rigidbody2D
        _rb2D = GetComponent<Rigidbody2D>();
        //by storing the reciprocal of the move time we ca use it by multiplying instead of dividing, this is more efficient
        _inverseMoveTime = 1f / MoveTime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="xDir">x direction</param>
    /// <param name="yDir">y direction</param>
    /// <param name="hit">for checking the collision</param>
    /// <returns>True if it is able to move. False if not.</returns>
    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        //store position to move from, based on cvurrent transform position
        Vector2 start = transform.position;
        //calculate end position based on the direction parameters passed in
        Vector2 end = start + new Vector2(xDir, yDir);

        //disable the boxCollider so that linecast doesn't hit this object's own collider
        _boxCollider.enabled = false;
        //cast a line from start point to end point checking collision on blockinglayer
        hit = Physics2D.Linecast(start, end, BlockingLayer);
        //re-enebale boxCollider after linecast
        _boxCollider.enabled = true;

        //check if anything was hit
        if (hit.transform == null)
        {
            //if nothing was hit, start SmoothMovement co-routine passing in the Vector2 end as destination
            StartCoroutine(SmoothMovement(end));

            //return true to say that Move was successful
            return true;
        }

        //if something was hit, return false. Move unsuccessful
        return false;
    }

    /// <summary>
    /// Co-routine for moving units from one space to next.
    /// </summary>
    /// <param name="end">Where to move.</param>
    /// <returns></returns
    protected IEnumerator SmoothMovement(Vector3 end)
    {
        //Calculate the remaining distance to move based on the square magnitude of the difference between current
        //position and end paramater. Square magnitue is used insted of magnitude because it's computationally cheaper.
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        //while that distance is greater than a very small amount (Epsilon, almost zero)
        while (sqrRemainingDistance > float.Epsilon)
        {
            //find a new position proportionally closer to the end, based on the moveTime
            Vector3 newPosition = Vector3.MoveTowards(_rb2D.position, end, _inverseMoveTime * Time.deltaTime);
            //call MovePosition on attached Rigidbody2D and move it to the calculated position
            _rb2D.MovePosition(newPosition);
            //recalculate the remaining distnace after moving
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            //return and loop until sqrRemainingDistance is close enought to zero to end the function
            yield return null;
        }
    }

    /// <summary>
    /// Virtual function (can be overridden by inheriting classes) to attemnt a move.
    /// </summary>
    /// <typeparam name="T">Component we expect ou unit to iteract with if blocked (player for enemies, wall for player).</typeparam>
    /// <param name="xDir">x direction</param>
    /// <param name="yDir">y direction</param>
    protected virtual void AttemptMove<T>(int xDir, int yDir)
        where T : Component
    {
        //store whatever our linecast hits when Move is called
        RaycastHit2D hit;
        //set canMove to true if Move was successful, false if failed
        bool canMove = Move(xDir, yDir, out hit);

        //check if nothing was hit by linecast
        if (hit.transform == null)
            //if nothing was hit, return and don't execute further code
            return;

        //get a component reference to the component of type T attached to the object that was hit
        T hitComponent = hit.transform.GetComponent<T>();

        //if canMove is false and hitComponent is not equeal to null, meaning MovingObject 
        //is blocked and has hit something it can interact with.
        if (!canMove && hitComponent != null)
            //call OnCantMove function and pass it hitComponent as a parameter
            OnCantMove(hitComponent);
    }

    /// <summary>
    /// The abstract modifier indicates that the thing being modified has a missing or incomplete implementantion.
    /// OnCantMove will be overriden by functions in the inheriting classes.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="component"></param>
    protected abstract void OnCantMove<T>(T component)
        where T : Component;
}
